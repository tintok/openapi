## REST Documentation

Interactive OpenAPI documentation is available over [here](https://tintok.gitlab.io/openapi/).

## User Manual and General Documentation

User Manual and general Documentation is available over [here](Documentation.pdf).