# General
API for the IPTK Project of Group J - TinTok


## HTTP Methods

TinTok tries to comply with standard HTTP and REST conventions as closely as possible in its use of HTTP methods (verbs).

| Method | Usage                                                                       |
| ------ | --------------------------------------------------------------------------- |
| GET    | Used to retrieve a resource                                                 |
| POST   | Used to create a new resource                                               |
| PUT    | Used to replace an existing resource. Does not update associated resources. |
| DELETE | Used to delete an existing resource                                         |


## HTTP Status Codes

TinTok tries to comply with standard HTTP and REST conventions as closely as possible in its use of HTTP status codes. All response bodies will include an error providing further information. If the `Accept-Language` is present we try to return the response in the specified language. Currentlly only English and German
are supported.

| Status code               | Usage                                                                                                             |
| ------------------------- | ----------------------------------------------------------------------------------------------------------------- |
| 200 OK                    | The request completed successfully                                                                                |
| 201 Created               | A new resource has been created successfully. The resource’s URI is available from the response’s Location header |
| 204 No Content            | An update to an existing resource has been applied successfully, including deletes                                |
| 206 Partial Content       | Indicates after a client request with a `Range` header the success of only returning partiall data                |
| 400 Bad Request           | The request was malformed                                                                                         |
| 401 Unauthrorized         | The User is not authorized                                                                                        |
| 403 Forbidden             | The User is not allowed to access this resource                                                                   |
| 404 Not Found             | The requested resource does not exist                                                                             |
| 405 Method Not Allowed    | The method is not allowed                                                                                         |
| 406 Not Acceptable        | Only JSON is supported, if the client uses a different type of `Content-Type` this error is returned              |
| 409 Conflic               | The state of the Resource prevents the execution of the request                                                   |
| 411 Length Required       | If the client did not provide a `Content-Length` field                                                            |
| 413 Payload To Large      | The uploaded data or the request is bigger then 55 MB                                                             |
| 416 Range Not Satisfiable | If the client used an unsatisfiablle `Range` header                                                               |
| 422 Unprocessable Entity  | The uploaded data is syntactically correct but contained semantic errors                                          |
| 502 Bad Gateway           | API Gateway is online but the server is offline ...                                                               |
| 5XX Internal Server Error | ... for every other 500er we don't know what went wrong, but be sure somebody is getting fired for that           |

## Error Representation

We use the following error structur for returning errors:

| Field     | Type                     | Description                                                                                                                                       |
| --------- | ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| timestamp | datetime                 | timestamp when the error occured                                                                                                                  |
| status    | Integer                  | http status code                                                                                                                                  |
| error     | String                   | http reason phrase                                                                                                                                |
| message   | String or List of Object | In most of the cases this is a String describing the error, for constraint violations this is an object containg field name and error description |
| path      | String                   | path were the error occured                                                                                                                       |


## HTTP Headers

TinTok uses the following headers:

| Header           | Description                                                                                                           |
| ---------------- | --------------------------------------------------------------------------------------------------------------------- |
| Content-Encoding | Responses are always served as UTF-8 encoded                                                                          |
| Caching          | Rresponses contain an apropriate caching header                                                                       |
| Content-Type     | Only JSON is supported                                                                                                |
| ETag             | ETags are used to help the client optimize requests                                                                   |
| If-None-Match    | Used in combination with `ETag` header to only fetch a resource if the resource changed                               |
| Accept-Language  | English and German are supported, if no Accept-Language header is present english is picked                           |
| Accept           | Is ignored, response is always served as JSON                                                                         |
| Location         | Returned in case of a POST create response. It indicates for the client were the newlly created resource is awaylable |
| Range            | On endpoints were data like an image or video is returned this can be used by the client to retrieve data partially   |


## JWT Claims

Besides the default JWT claims `sub`, `nbf`, `iss`, `exp`, `iat` we include the following custom claims:

### Custom Claims

| Claim Name | Description              | Example                              |
| ---------- | ------------------------ | ------------------------------------ |
| id         | The token owners User id | 5982feee-64b7-11eb-ae93-0242ac130002 |
