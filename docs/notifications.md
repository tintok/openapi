## FCM Notification Data

There are two types of Notifications:
 - `MATCH` 
 - `CHAT`

The Notifications data Map contains the following keys and values:


### Chat Notification

Chat Notifications are triggered if a User send a Chat message to another user.

#### Data

| Field Name | Description               | Example                              |
| ---------- | ------------------------- | ------------------------------------ |
| id         | The senders User id       | 5982feee-64b7-11eb-ae93-0242ac130002 |
| name       | The senders full name     | Elliot Alderson                      |
| messageId  | The messages id           | 13439a06-97c2-47fe-b167-6bc87675aed1 |
| message    | The chat messages content | Hey, what's up?                      |
| type       | is always `CHAT`          | CHAT                                 |


### Match Notification

Match Notifications are triggered if two Users match.

#### Data

| Field Name | Description               | Example                              |
| ---------- | ------------------------- | ------------------------------------ |
| id         | The matches User id       | 5982feee-64b7-11eb-ae93-0242ac130002 |
| name       | The matches full name     | Elliot Alderson                      |
| type       | is always `MATCH`         | MATCH                                |